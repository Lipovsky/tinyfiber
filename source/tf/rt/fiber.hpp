#pragma once

#include <tf/rt/fwd.hpp>
#include <tf/rt/routine.hpp>
#include <tf/rt/stack.hpp>
#include <tf/rt/id.hpp>
#include <tf/rt/watcher.hpp>

#include <sure/context.hpp>

#include <wheels/intrusive/list.hpp>

namespace tf::rt {

struct Fiber final
    : private sure::ITrampoline,
      public wheels::IntrusiveListNode<Fiber> {

  Scheduler* scheduler;
  FiberRoutine routine;
  Stack stack;
  sure::ExecutionContext context;
  FiberId id;
  IFiberWatcher* watcher{nullptr};

  ~Fiber();

  Fiber(Scheduler* scheduler,  //
        FiberRoutine routine,  //
        Stack stack,     //
        FiberId id);

 private:
  // sure::ITrampoline
  [[noreturn]] void Run() noexcept override;
};

}  // namespace tf::rt
