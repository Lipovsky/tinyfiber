# Tests

set(TESTS_TARGET ${LIB_TARGET}_tests)

file(GLOB TESTS_CXX_SOURCES *.cpp)

add_executable(${TESTS_TARGET} ${TESTS_CXX_SOURCES})
target_link_libraries(${TESTS_TARGET} ${LIB_TARGET})

add_test(NAME ${TESTS_TARGET}
        COMMAND ${TESTS_TARGET})

set_tests_properties(${TESTS_TARGET} PROPERTIES LABELS "test")
