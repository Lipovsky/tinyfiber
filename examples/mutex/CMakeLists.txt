set(EXAMPLE_TARGET ${LIB_TARGET}_example_mutex)

add_executable(${EXAMPLE_TARGET} main.cpp)
target_link_libraries(${EXAMPLE_TARGET} ${LIB_TARGET})

add_test(NAME ${EXAMPLE_TARGET}
        COMMAND ${EXAMPLE_TARGET})

set_tests_properties(${EXAMPLE_TARGET} PROPERTIES LABELS "example")
