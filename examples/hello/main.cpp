#include <tf/run.hpp>
#include <tf/sched/yield.hpp>
#include <tf/sched/spawn.hpp>

#include <fmt/core.h>

using tf::RunScheduler;
using tf::Spawn;
using tf::Yield;
using tf::JoinHandle;

int main() {
  RunScheduler([] {
    fmt::println("Parent");

    JoinHandle child = Spawn([] {
      fmt::println("Child");

      for (size_t i = 0; i < 3; ++i) {
        fmt::println("Step #{}", i+1);
        tf::Yield();
      }
    });

    child.Join();
    fmt::println("Child completed");
  });

  return 0;
}
